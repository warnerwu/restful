<?php
/**
 * Created by PhpStorm.
 * User: warnerwu
 * Date: 2018/3/25
 * Time: 下午5:19
 */

class ErrorCode {

    /**
     * -----------------------------------------------------------------------
     * 用户管理错误码常量定义
     * -----------------------------------------------------------------------
     */

    // 用户名已存在
    const USERNAME_EXISTS = 1;
    // 密码不能为空
    const PASSWORD_CANNOT_EMPTY = 2;
    // 用户名不能为空
    const USERNAME_CANNOT_EMPTY = 3;
    // 用户注册失败
    const REGISTER_FAIL = 4;
    // 用户名或密码无效
    const USERNAME_OR_PASSWORD_INVALID = 5;

    /**
     * -----------------------------------------------------------------------
     * 文章管理错误码常量定义
     * -----------------------------------------------------------------------
     */

    // 文章标题不能为空
    const ARTICLE_TITLE_CANNOT_EMPTY = 6;
    // 文章内容不能为空
    const ARTICLE_CONTENT_CANNOT_EMPTY = 7;
    // 用户ID不能为空
    const USER_ID_CANNOT_EMPTY = 8;
    // 添加文章失败
    const ARTICLE_CREATE_FAIL = 9;
    // 文章ID不能为空
    const ARTICLE_ID_CANNOT_EMPTY = 10;
    // 文章记录不存在
    const ARTICLE_NOT_FOUND = 11;
    // 无权操作
    const PERMISSION_DENIED = 12;
    // 文章更新失败
    const ARTICLE_EDIT_FAIL = 13;
    // 文章删除失败
    const ARTICLE_DELETE_FAIL = 14;
    // 文章分页太大
    const PAGE_SIZE_TO_BIG = 15;

}