<?php
/**
 * 用户管理
 *
 * Created by PhpStorm.
 * User: warnerwu
 * Date: 2018/3/25
 * Time: 下午3:40
 */

require_once __DIR__ . '/ErrorCode.php';
require_once __DIR__ . '/Tools.php';


class User {

    /**
     * 数据库连接句柄
     *
     * @var
     */
    private $_db;

    /**
     * 构造方法
     *
     * @param PDO $_db PDO数据库连接句柄
     */
    public function __construct($_db) {
        $this->_db = $_db;
    }

    /**
     * 抛出异常方法
     *
     * @param string $message
     * @param string $errorCode
     * @throws Exception
     */
    private function _error( $message, $errorCode ) {
        throw new Exception( $message, $errorCode);
    }

    /**
     * 用户登录
     *
     * @param string $username 用户名
     * @param string $password 密码
     * @return mixed 用户信息
     * @throws Exception
     */
    public function login( $username, $password ) {

        // 检测用户名是否为空
        if ( empty( $password ) ) {
            $this->_error( '用户名不能为空', ErrorCode::USERNAME_CANNOT_EMPTY );
        }

        // 检测密码是否为空
        if ( empty( $password ) ) {
            $this->_error( '密码不能为空', ErrorCode::PASSWORD_CANNOT_EMPTY );
        }

        // 用户登录读取数据库用户表记录
        $sql = 'SELECT * FROM `user` WHERE `username` = :username AND `password` = :password';

        // 查询记录变量处理
        $password = $this->_md5( $password );

        // 执行SQL预处理返回PDOStatement对象
        $stmt = $this->_db->prepare( $sql );

        // 执行SQL查询变量绑定
        $stmt->bindParam( ':username', $username );
        $stmt->bindParam( ':password', $password );

        // 执行PDO查询用户记录
        $stmt->execute();

        // 获取用户记录
        $user = $stmt->fetch( PDO::FETCH_ASSOC );
        if ( empty( $user ) ) {
            $this->_error( '用户名或密码错误', ErrorCode::USERNAME_OR_PASSWORD_INVALID );
        }

        // 删除用户密码敏感数据
        unset( $user['password'] );

        // 返回用户信息数据
        return $user;
    }

    /**
     * 用户注册
     *
     * @param string $username  用户名
     * @param string $password  密码
     * @return array  用户注册信息
     * @throws Exception
     */
    public function register( $username, $password ) {
        // 检测用户名是否为空
        if ( empty( $password ) ) {
            $this->_error( '用户名不能为空', ErrorCode::USERNAME_CANNOT_EMPTY );
        }

        // 检测密码是否为空
        if ( empty( $password ) ) {
            $this->_error( '密码不能为空', ErrorCode::PASSWORD_CANNOT_EMPTY );
        }

        // 检测用户名是否存在
        if ( $this->_isUsernameExists( $username ) ) {
            $this->_error( '用户名已存在', ErrorCode::USERNAME_EXISTS );
        }

        // 用户注册写入数据库用户表记录
        $sql = 'INSERT INTO `user` (`username`,`password`, `created_at`) VALUES (:username, :password, :created_at)';

        // 变量入库准备
        $created_at = Tools::getFormatDatetime( time() );
        $password = $this->_md5( $password );

        // 执行SQL预处理返回PDOStatement对象
        $stmt = $this->_db->prepare( $sql );

        // 执行SQL查询变量绑定
        $stmt->bindParam( ':username', $username );
        $stmt->bindParam( ':password', $password );
        $stmt->bindParam( ':created_at', $created_at );

        // 执行PDO查询写入记录
        if ( !$stmt->execute() ) {
            $this->_error( '注册失败', ErrorCode::REGISTER_FAIL );
        }

        // 返回用户注册信息
        return [
            'user_id' => $this->_db->lastInsertId(),
            'username' => $username,
            'created_at' => $created_at
        ];
    }

    /**
     * 用户密码MD5加密
     *
     * @param string $password  原始密码
     * @param string $key   盐值
     * @return string   加密密码字符串
     */
    private function _md5( $password, $key = 'api.mac.test.register' ) {
        return md5( $password . $key );
    }


    /**
     * 检测用户名是否存在
     *
     * @param string $username 用户密码
     * @return bool 是否存在用户名布尔值
     */
    private function _isUsernameExists( $username ) {
        // 查询用户
        $sql = 'SELECT * FROM `user` WHERE `username` = :username';

        // 执行SQL预处理返回PDOStatement对象
        $stmt = $this->_db->prepare( $sql );

        // 执行SQL查询变量绑定
        $stmt->bindParam( ':username', $username );

        // 执行PDO查询
        $stmt->execute();

        // 以数组的形式返回查询结果
        $user = $stmt->fetch( PDO::FETCH_ASSOC );

        // 返回判断用户是否存在Bool值
        return !empty( $user );
    }
}