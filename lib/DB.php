<?php
/**
 * 连接数据库并返回数据库连接句柄
 *
 * Created by PhpStorm.
 * User: warnerwu
 * Date: 2018/3/25
 * Time: 下午3:40
 */


// 实例化PDO
$pdo = new PDO( 'mysql:host=localhost;dbname=api', 'root', 'root' );

// 设置PDO预处理语句模拟为禁用
$pdo->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );

// 返回数据库连接句柄
return $pdo;
