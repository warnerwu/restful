<?php
/**
 * Created by PhpStorm.
 * User: warnerwu
 * Date: 2018/3/25
 * Time: 下午5:56
 */

class Tools {
    /**
     * 返回格式化时间格式
     *
     * @param integer $time 时间戳
     * @return false|string 格式化时间, 如: 2018-03-25 18:00:22
     */
    public static function getFormatDatetime( $time ) {
        return date( 'Y-m-d H:i:s', $time );
    }

    /**
     * 打印调试
     *
     * @param array $arr 待输出字符串
     * @param string $type 可选数据类型有[json|array]
     */
    public static function P( $arr, $type = 'json' ) {
        if ( $type == 'json' ) {
            echo json_encode( $arr );
        } else {
            echo '<pre />';
            echo '<hr color="red" />';
            print_r( $arr );
            echo '<hr color="red" />';
        }
    }
}