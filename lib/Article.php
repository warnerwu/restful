<?php
/**
 * 文章管理
 *
 * Created by PhpStorm.
 * User: warnerwu
 * Date: 2018/3/25
 * Time: 下午8:01
 */

require_once __DIR__ . '/Tools.php';
require_once __DIR__ . '/ErrorCode.php';

class Article {

    /**
     * 数据库连接句柄
     *
     * @var
     */
    private $_db;

    /**
     * 构造方法
     *
     * @param PDO $_db PDO数据库连接句柄
     */
    public function __construct($_db) {
        $this->_db = $_db;
    }

    /**
     * 抛出异常方法
     *
     * @param string $message
     * @param string $errorCode
     * @throws Exception
     */
    private function _error( $message, $errorCode ) {
        throw new Exception( $message, $errorCode);
    }

    /**
     * 添加文章
     *
     * @param string $title 文章标题
     * @param string $content 文章内容
     * @param string $user_id 用户ID
     * @return array
     * @throws Exception
     */
    public function create( $title, $content, $user_id ) {
        // 检测文章标题是否为空
        if ( empty( $title ) ) {
            $this->_error( '标题不能为空', ErrorCode::ARTICLE_TITLE_CANNOT_EMPTY );
        }

        // 检测文章内容是否为空
        if ( empty( $content ) ) {
            $this->_error( '文章内容不能为空', ErrorCode::ARTICLE_CONTENT_CANNOT_EMPTY );
        }

        // 检测用户ID是否为空
        if (  empty( $user_id ) ) {
            $this->_error( '用户ID不能为空', ErrorCode::USER_ID_CANNOT_EMPTY );
        }

        // 文章添加写入文章表记录
        $sql = 'INSERT INTO `article` (`title`, `content`, `user_id`, `created_at`) VALUES( :title, :content, :user_id, :created_at )';

        // 变量入库准备
        $created_at = Tools::getFormatDatetime( time() );

        // 执行SQL预处理返回PDOStatement对象
        $stmt = $this->_db->prepare( $sql );

        // 执行PDOStatement对象插入记录变量绑定
        $stmt->bindParam( ':title', $title );
        $stmt->bindParam( ':content', $content );
        $stmt->bindParam( ':user_id', $user_id );
        $stmt->bindParam( ':created_at', $created_at );

        // 执行PDOStatement对象插入记录操作
        if ( !$stmt->execute() ) {
            $this->_error( '添加文章失败', ErrorCode::ARTICLE_CREATE_FAIL );
        }

        // 返回文章添加信息
        return [
            'article_id' => $this->_db->lastInsertId(),
            'title' => $title,
            'content' => $content,
            'created_at' => $created_at
        ];
    }

    /**
     * 编辑文章
     *
     * @param int $article_id 文章ID
     * @param string $title 文章标题
     * @param string $content 文章内容
     * @param int $user_id 用户ID
     * @return array|mixed
     * @throws Exception
     */
    public function edit( $article_id, $title, $content, $user_id ) {
        // 查询指定文章记录
        $article = $this->view( $article_id );

        // 检测用户名是否是当前文章的所有者
        if ( $article['user_id'] != $user_id ) {
            $this->_error( '您无权编辑该文章', ErrorCode::PERMISSION_DENIED );
        }

        // 如果标题为空, 则使用原有文章标题
        $title = empty( $title ) ? $article['title'] : $title;
        // 如果内容为空, 则使用原有文章内容
        $content = empty( $content ) ? $article['content'] : $content;

        // 如果标题和原文章标题相同并且内容和原文章内容相同则说明没有进行编辑操作, 则直接返回文章内容
        if ( $title === $article['title'] && $content === $article['content'] ) {
            // 返回文章记录信息
            return $article;
        }

        // 更新文章更新文章表记录
        $sql = 'UPDATE `article` SET `title` = :title, `content` = :content WHERE `article_id` = :article_id';

        // 执行SQL预处理操作返回PDOStatement对象
        $stmt = $this->_db->prepare( $sql );

        // 执行PDOStatement对象更新参数绑定
        $stmt->bindParam( ':title', $title );
        $stmt->bindParam( ':content', $content );
        $stmt->bindParam( ':article_id', $article_id );

        // 执行PDOStatement对象更新操作
        if ( !$stmt->execute() ) {
            $this->_error( '文章编辑失败', ErrorCode::ARTICLE_EDIT_FAIL );
        }

        // 返回更新后的文章记录信息
        return [
            'article_id' => $article_id,
            'title' => $title,
            'content' => $content,
            'created_at' => $article['created_at']
        ];
    }

    /**
     * 通过文章ID查询指定文章记录
     *
     * @param int $article_id 文章ID
     * @return mixed 文章记录
     * @throws Exception
     */
    public function view( $article_id ) {
        if ( empty( $article_id ) ) {
            $this->_error( '文章ID不能为空', ErrorCode::ARTICLE_ID_CANNOT_EMPTY );
        }

        // 查询指定文章记录ID的文章
        $sql = 'SELECT * FROM `article` WHERE `article_id` = :article_id';

        // 执行SQL预处理返回PDOStatement对象
        $stmt = $this->_db->prepare( $sql );

        // 执行PDOStatement对象查询参数绑定
        $stmt->bindParam( ':article_id', $article_id );

        // 执行PDOStatement对象查询操作
        $stmt->execute();

        // 通过PDOStatement对象获取查询数据
        $article = $stmt->fetch( PDO::FETCH_ASSOC );

        // 文章不存在则抛出异常
        if ( empty( $article ) ) {
            $this->_error( '文章不存在', ErrorCode::ARTICLE_NOT_FOUND );
        }

        // 文章存在则返回文章记录数据
        return $article;
    }

    /**
     * 删除文章
     *
     * @param int $article_id 文章ID
     * @param int $user_id 用户ID
     * @return bool
     * @throws Exception
     */
    public function delete( $article_id, $user_id ) {
        // 通过指定文章ID查询文章
        $article = $this->view( $article_id );

        // 检测操作用户是否是文章所有者
        if ( $article['user_id'] !== $user_id ) {
            $this->_error( '您无权删除文章', ErrorCode::PERMISSION_DENIED );
        }

        // 删除记录删除文章表文章记录
        $sql = 'DELETE FROM `article` WHERE `article_id` = :article_id AND `user_id` = :user_id';

        // 执行SQL预处理操作返回PDOStatement对象
        $stmt = $this->_db->prepare( $sql );

        // 执行PDOStatement对象删除记录参数绑定
        $stmt->bindParam( ':article_id', $article_id );
        $stmt->bindParam( ':user_id', $user_id );

        // 执行PDOStatement对象删除记录操作
        if ( !$stmt->execute() ) {
            $this->_error( '删除失败', ErrorCode::ARTICLE_DELETE_FAIL );
        }

        return true;
    }

    /**
     * 获取某个用户下的文章列表
     *
     * @param int $user_id 用户ID
     * @param int $page 页码
     * @param int $size 每页文件记录数
     * @return array 文章分页数据结果集
     * @throws Exception
     */
    public function getList( $user_id, $page = 1, $size = 20 ) {

        // 分页大小限制
        if ( $size > 100 ) {
            $this->_error( '分页大小最大为100', ErrorCode::PAGE_SIZE_TO_BIG);
        }

        // 查询文章列表
        $sql = 'SELECT * FROM `article` ';
        // 查询指定用户所有者的文章, 否则查询返回文章
        if ( !empty( $user_id ) ) {
            $sql .= 'WHERE `user_id` = :user_id ';
        }
        // 获取所有的文章列表
        $sql .= 'LIMIT :limit, :offset';


        // 分页起始位置处理
        $limit = ( $page - 1 ) * 10;
        $limit = ( $limit < 0 ) ? 0 : $limit;


        // 执行SQL预处理操作返回PDOStatement对象
        $stmt = $this->_db->prepare( $sql );


        // 执行PDOStatement对象查询记录参数绑定
        $stmt->bindParam( ':limit', $limit );
        $stmt->bindParam( ':offset', $size );
        // 如果用户ID存在时绑定用户ID
        if ( !empty( $user_id ) ) {
            $stmt->bindParam( ':user_id', $user_id );
        }

        // 执行PDOStatement对象查询操作
        $stmt->execute();

        // 返回文章数据集
        return $stmt->fetchAll( PDO::FETCH_ASSOC );
    }

}